# IPv6 resources

## Books / Documentation

* [https://github.com/becarpenter/book6](https://github.com/becarpenter/book6)
* [List of all IPv6 related RFCs](https://www.system.de/ipv6-rfc/)

### Address Planing

* [https://blog.apnic.net/2023/04/04/ipv6-architecture-and-subnetting-guide-for-network-engineers-and-operators/](https://blog.apnic.net/2023/04/04/ipv6-architecture-and-subnetting-guide-for-network-engineers-and-operators/)
* [https://www.daryllswer.com/ipv6-architecture-and-subnetting-guide-for-network-engineers-and-operators/](https://www.daryllswer.com/ipv6-architecture-and-subnetting-guide-for-network-engineers-and-operators/)

## Cloud

### AWS

* [AWS IPv6 gaps](https://github.com/DuckbillGroup/aws-ipv6-gaps/)
* [Introducing IPv6-only subnets and EC2 instances](https://aws.amazon.com/blogs/networking-and-content-delivery/introducing-ipv6-only-subnets-and-ec2-instances/)
* [AWS service endpoints by region and IPv6 support](https://awsipv6.neveragain.de)
* [AWS IPv6 Fundamentals and VPC Connectivity](https://explore.skillbuilder.aws/learn/course/external/view/elearning/20489/aws-ipv6-fundamentals-and-vpc-connectivity)

### OpenStack

* [Use-case: Global IPv6 connectivity for tenant networks](https://docs.openstack.org/neutron-dynamic-routing/latest/install/usecase-ipv6.html)

## Governments and IPv6
* [Czech Republic sets IPv4 end date](https://konecipv4.cz/en/)
* [German Federal IPv6 Program (German)](https://www.bdbos.bund.de/DE/Aufgaben/IPv6/ipv6programm_node.html)
* [RIPE84 presentation on deploying IPv6  in Public Authorities in Germany](https://ripe84.ripe.net/archives/video/740/)
* [Malaysia](https://mcmc.gov.my/skmmgovmy/media/General/Resources/PCP-Proposed-Way-Forward-on-100-Adoption-of-IPv6.pdf)
* [US Government Memorandum](https://www.whitehouse.gov/wp-content/uploads/2020/11/M-21-07.pdf)
* [US DoD INSTRUCTION 8440.02](https://www.esd.whs.mil/Portals/54/Documents/DD/issuances/dodi/844002p.PDF)
* [Vietnam plans to convert all its networks to IPv6 by 2030](https://datafiles.chinhphu.vn/cpp/files/vbpq/2024/9/1132-ttg.signed.pdf)

## Statistics

* [APNIC - IPv6 Capable Rate by country (%)](https://stats.labs.apnic.net/ipv6)
* [Akamai](https://www.akamai.com/de/internet-station/cyber-attacks/state-of-the-internet-report/ipv6-adoption-visualization)
* [Cisco 6Lab](https://6lab.cisco.com)
* [Cloudflare Radar](https://radar.cloudflare.com/reports/ipv6)
* [Facebook](https://www.facebook.com/ipv6)
* [Google IPv6](https://www.google.com/ipv6)
* [Hurricane Electric IPv6 Progress Report](https://bgp.he.net/ipv6-progress-report.cgi)
* [Potaroo BGP](https://bgp.potaroo.net/index-v6.html)
* [Top 1M IPv6 Connectivity](https://www.employees.org/~dwing/aaaa-stats/)
* [China IPv6](https://m.china-ipv6.cn/complete/) (Chinese)

## Misc

* [IPv6-Buzz Podcast](https://packetpushers.net/podcast/ipv6-buzz/)

## Tools

### CLI

* [sipcalc](https://github.com/sii/sipcalc)
* [ipv6gen](https://github.com/vladak/ipv6gen)
* [aggregate6](https://github.com/job/aggregate6)
* [grepcidr](https://github.com/ryantig/grepcidr)

## Training / Certification

* [Hurricane Electric](https://ipv6.he.net/certification/)
* [RIPE IPv6 Webinars](https://learning.ripe.net/w/)
* [AFRINIC Academy](https://afrinic.academy/)
* [AWS IPv6 Fundamentals and VPC Connectivity](https://explore.skillbuilder.aws/learn/course/external/view/elearning/20489/aws-ipv6-fundamentals-and-vpc-connectivity)

## Videos

* [Klara Mall - IPv6 am KIT: Erfahrungsbericht und aktueller Status (German)](https://opencast.hu-berlin.de/paella/ui/watch.html?id=2fdf575e-eec7-4131-88fa-4b8f39c0e6d6)
* Networking Basics by DE-CIX Academy: 03 - The Internet Protocol
  * [Video in English](https://youtu.be/0dLR_tU1z7M)
  * [PDF of slides](https://www.de-cix.net/_Resources/Persistent/f/0/3/2/f0327ac6250974b0e2cf2544dfae9b46e6e770f7/DE-CIX%20Academy%20Handout_Networking%20Basics%2003_The%20Internet%20Protocol_handout.pdf)
* [Free CCNA IPv6 Part 1](https://www.youtube.com/watch?v=ZNuXyOXae5U)
* [Free CCNA IPv6 Part 2](https://www.youtube.com/watch?v=BrTMMOXFhDU)

## Issues

* [https://gitlab.com/interlan/webbserver-ipv6-only](https://gitlab.com/interlan/webbserver-ipv6-only)

## Fun

* [IPv6 Excuses](https://ipv6excuses.com/)
* [IPv6 Bingo](https://ipv6bingo.com/)
* [NATs are good](https://www.youtube.com/watch?v=v26BAlfWBm8&)
* [SixSpotting](https://game.flyingpenguintech.org/)
* wumpus.quest: traceroute -f 8 -q 1 wumpus.quest
